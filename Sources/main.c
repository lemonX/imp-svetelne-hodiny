#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */

#define LOGIN_SIZE 64 // Bytes

#define SCREEN_SIZE_X 32 // Screen columns
#define SCREEN_SIZE_Y 8  // Screen rows

unsigned char login[LOGIN_SIZE] = {0, 66, 36, 24, 24, 36, 66, 0, 0, 62, 32, 30, 16, 16, 14, 0, 0, 28, 42, 42, 42, 42, 18, 0, 0, 62, 16, 16, 16, 16, 14, 0, 0, 28, 34, 34, 34, 34, 34, 0, 0, 62, 16, 16, 16, 16, 14, 0, 0, 60, 70, 74, 82, 98, 60, 0, 0, 60, 70, 74, 82, 98, 60, 0};

unsigned char *screen = (char *) 0xB0; // Start of memmory maped to diods

//Mapped controls (buttons and slider)
unsigned char *init = (char *) 0xD0;
unsigned char *horiz = (char *) 0xD1;
unsigned char *vert = (char *) 0xD2;
unsigned char *speed = (char *) 0xD3;

typedef enum {
	INIT, HORIZ, VERT
} State;

State state = INIT;

//Functions declarations
void wait(void);
void show(int, int);
void listenKeys(void);


// Make delay between screen change iterations
void wait(void) {
	//Empty loops to make delay in program execution
	int i, j;
	int brake = 260 - *speed;

	for (i = 0; i <= brake; i++ ) {
		for (j = 0; j <= 50; j++ )
			listenKeys();
	}
}

// Redraw whole screen with given rotation
void show(int deltaX, int deltaY) {
	int i, index;
	unsigned char col;

	for(i = 0; i < SCREEN_SIZE_X; i++) {
		// Horizontal shift
		index = (i + deltaX) % LOGIN_SIZE;
		col = login[index];

		// Vertical shift
		col = (col >> deltaY) | (col << (sizeof(col)*8 - deltaY));

		// Write to screen 
		screen[i] = col;
	}
}

// Check buttons press and set state variable
void listenKeys(void) {
	// Key listener
	if (*horiz) {
		state = HORIZ;
	} else if (*vert) {
		state = VERT;
	} else if (*init) {
		state = INIT;
	}
}


void main(void) {
	int i = 0;
	int deltaX = 0;
	int deltaY = 0;

	EnableInterrupts;

	*init = *horiz = *vert = 0;
	*speed = 150;

	for(;;) {
		__RESET_WATCHDOG();

		listenKeys(); //Function is setting global "state"

		// Determine action
		if (state == HORIZ) {
			deltaX = ++deltaX % LOGIN_SIZE;
		} else if (state == VERT) {
			deltaY = ++deltaY % SCREEN_SIZE_Y;
		} else if (state == INIT) {
			deltaX = 0;
			deltaY = 0;
		}

		show(deltaX, deltaY);

		wait();
	}
}
